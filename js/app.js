var app = app || {};

(function ($) {
    // -- ELEMENTS
    app.el = app.el || {};
    app.el.$team = $('.team');
    app.el.$members = $('.member');

    // -- CONFIG
    app.cfg = app.cfg || {};

    app.cfg.membersCount = function () {
        return parseInt( app.el.$members.length );
    }

    app.cfg.teamWidth = function () {
        return parseInt( app.el.$team.width() );
    }

    app.cfg.memberWidth = function () {
        return parseInt( app.el.$members.first().width() );
    }

    app.cfg.cols = function () {
        return parseInt( Math.floor( app.cfg.teamWidth() / app.cfg.memberWidth() ) );
    }

    // -- UTILS
    app.util = app.util || {};
    app.util.getCords = function (position) {
        return {
            x: Math.floor( position % app.cfg.cols() ), // col
            y: Math.floor( position / app.cfg.cols() ) // row
        };
    }

    app.util.setClass = function(cords) {
        app.el.$members.each(function(k) {
            var $this = $(this);
            var position = k;
            var thisCords = app.util.getCords(position);
            var classApply = '';

            if (cords.y == thisCords.y) {
                classApply += 'center-';
            } else if (cords.y > thisCords.y) {
                classApply += 'bottom-';
            } else if (cords.y < thisCords.y) {
                classApply += 'top-';
            }

            if (cords.x == thisCords.x) {
                classApply += 'center';
            } else if (cords.x > thisCords.x) {
                classApply += 'right';
            } else if (cords.x < thisCords.x) {
                classApply += 'left';
            }

            $this.attr('class', 'member ' + classApply);
        });
    }

    // -- INIT
    app.el.$members
        .each(function(k) {
            $(this).data('position', k);
        })
        // .attr({ 'class': 'member center-center' })
    ;

    app.el.$members.on('mouseenter', function(e) {
        var $this = $(this);
        var position = parseInt($this.data('position'));
        var cords = app.util.getCords(position)
        console.log('Position:', position, 'Coords:', cords);
        app.util.setClass(cords);
    });
})(jQuery);
